FROM python:alpine

WORKDIR /app

# Install Requirement
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Copy All Files
COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
