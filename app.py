from flask import Flask, jsonify, make_response

app = Flask(__name__)

@app.route("/")
def helloworld():
    return "Hello World!!"

@app.route("/healthcheck")
def heathcheck():
    return make_response(jsonify(
        success=True,
    ), 200)


if __name__ == "__main__":
    app.run()